package org.enoy.awesomeradio.music.impl;

import java.util.Arrays;
import java.util.List;

import org.enoy.awesomeradio.music.Album;
import org.enoy.awesomeradio.music.Song;

public class SimpleAlbum implements Album {

	private String title;
	private List<Song> songs;
	
	public SimpleAlbum(String title, Song...songs) {
		this.title = title;
		this.songs = Arrays.asList(songs);
	}
	
	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public List<Song> getSongs() {
		return songs;
	}
	
}
