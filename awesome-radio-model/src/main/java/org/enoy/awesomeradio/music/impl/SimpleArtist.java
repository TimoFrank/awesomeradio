package org.enoy.awesomeradio.music.impl;

import org.enoy.awesomeradio.music.Artist;

public class SimpleArtist implements Artist {

	private String name;
	
	public SimpleArtist(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}

}
