package org.enoy.awesomeradio.music;

import java.util.List;

public interface Album {

	public String getTitle();
	public List<Song> getSongs();
	
}
