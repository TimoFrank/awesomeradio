package org.enoy.awesomeradio.music;

import java.util.List;

public interface Song {

	public String getId();
	public String getTitle();
	public List<Artist> getArtists();
	
}
