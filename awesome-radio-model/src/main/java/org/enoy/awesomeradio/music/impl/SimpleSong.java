package org.enoy.awesomeradio.music.impl;

import java.util.List;

import org.enoy.awesomeradio.music.Artist;
import org.enoy.awesomeradio.music.Song;

public class SimpleSong implements Song{

	private String id;
	private String title;
	private List<Artist> artists;

	public SimpleSong(String id, String title, List<Artist> artists) {
		this.id = id;
		this.title = title;
		this.artists = artists;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public List<Artist> getArtists() {
		return artists;
	}

}
