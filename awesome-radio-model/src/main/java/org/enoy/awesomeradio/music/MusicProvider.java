package org.enoy.awesomeradio.music;

import java.io.File;
import java.util.List;

public interface MusicProvider {

	public List<Song> searchSongs(String search);
	public List<Artist> searchArtists(String search);
	public List<Album> searchAlbums(String search);
	
	public File getSong(Song song);
	
}
