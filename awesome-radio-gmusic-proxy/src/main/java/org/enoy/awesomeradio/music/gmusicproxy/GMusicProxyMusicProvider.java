package org.enoy.awesomeradio.music.gmusicproxy;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.naming.OperationNotSupportedException;

import org.enoy.awesomeradio.music.Album;
import org.enoy.awesomeradio.music.Artist;
import org.enoy.awesomeradio.music.MusicProvider;
import org.enoy.awesomeradio.music.Song;
import org.enoy.gmusicproxy.bridge.GMusicProxyBridge;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GMusicProxyMusicProvider implements MusicProvider{

	@Value("${awesomeradio.music.provider.gmusicproxy.baseUrl}")
	private String proxyBaseUrl;
	@Value("${awesomeradio.music.provider.gmusicproxy.maxSearchTracks:-1}")
	private int numTracks;
	
	private GMusicProxyBridge bridge;
	
	@PostConstruct
	private void init(){
		this.bridge = new GMusicProxyBridge(proxyBaseUrl);
	}
	
	@Override
	public List<Song> searchSongs(String search) {
		String[][] result = search(null, search);

		for (String[] strings : result) {
			for (String string : strings) {
				System.out.println(string);
			}
		}
		
		return null;
	}

	private String[][] search(String artist, String title) {
		
		try{
			String[][] searchResult;
			if(numTracks == -1){
				searchResult = bridge.search(artist, title);
			}else{
				searchResult = bridge.search(artist, title, numTracks);
			}
			
			return searchResult;
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<Artist> searchArtists(String search) {
		
		String[][] result = search(search, null);

		for (String[] strings : result) {
			for (String string : strings) {
				System.out.println(string);
			}
		}
		
		return null;
	}

	@Override
	public List<Album> searchAlbums(String search) {
		throw new RuntimeException(new OperationNotSupportedException());
	}

	@Override
	public File getSong(Song song) {
		return null;
	}

	
	
}
