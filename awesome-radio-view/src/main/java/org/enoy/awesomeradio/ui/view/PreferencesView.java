package org.enoy.awesomeradio.ui.view;

import javax.annotation.PostConstruct;

import org.enoy.awesomeradio.ui.AwesomeRadioUI;
import org.vaadin.spring.navigator.annotation.VaadinView;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;

@VaadinView(name = AwesomeRadioUI.PREFERENCES_VIEW)
public class PreferencesView extends HorizontalLayout implements View
{
	private static final long serialVersionUID = -5503566262306825631L;

	@PostConstruct
	void init()
	{
		NativeButton b = new NativeButton("preferencesView");
		addComponent(b);
	}
	
	@Override
	public void enter(ViewChangeEvent event)
	{
	}
}
