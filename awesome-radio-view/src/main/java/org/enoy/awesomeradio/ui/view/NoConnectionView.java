package org.enoy.awesomeradio.ui.view;

import javax.annotation.PostConstruct;

import org.enoy.awesomeradio.ui.AwesomeRadioUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.navigator.annotation.VaadinView;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@VaadinView(name = AwesomeRadioUI.NO_CONNECTION_VIEW)
public class NoConnectionView extends VerticalLayout implements View
{
	private static final long serialVersionUID = -5566078276430900187L;
	
	@Autowired
	private AwesomeRadioUI ui;
	
	@PostConstruct
	public void init()
	{
		setSizeFull();
		
		Label noConn = new Label("no connection to central eureka server");
		noConn.setWidth(null);
		addComponent(noConn);
		setComponentAlignment(noConn, Alignment.MIDDLE_CENTER);
	}
	
	public void enter(ViewChangeEvent event)
	{
	}
}
