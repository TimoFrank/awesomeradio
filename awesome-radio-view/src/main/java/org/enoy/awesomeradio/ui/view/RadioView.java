package org.enoy.awesomeradio.ui.view;

import javax.annotation.PostConstruct;

import org.enoy.awesomeradio.ui.AwesomeRadioUI;
import org.vaadin.spring.navigator.annotation.VaadinView;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.VerticalLayout;

@VaadinView(name = AwesomeRadioUI.RADIO_VIEW)
public class RadioView extends VerticalLayout implements View
{
	private static final long serialVersionUID = -6298531735865882335L;

	@PostConstruct
	void init()
	{
		setSizeFull();
		
		HorizontalLayout header = new HorizontalLayout();
		header.setStyleName("caption");
		
		Label caption = new Label("Radio");
		caption.setStyleName("caption-label");
		header.addComponent(caption);
		header.setExpandRatio(caption, 0);
		
		Label motto = new Label("hearing awesome music..");
		motto.setStyleName("caption-motto-label");
		header.addComponent(motto);
		header.setExpandRatio(motto, 1);
		
		VerticalLayout radio = new VerticalLayout();
		radio.setStyleName("radio-area");
		radio.setSizeFull();
		radio.addComponent(new Label("radio"));
		
		VerticalLayout playlist = new VerticalLayout();
		playlist.setStyleName("playlist-area");
		playlist.setSizeFull();
		playlist.addComponent(new Label("playlist"));
		
		HorizontalSplitPanel radioPanel = new HorizontalSplitPanel(radio, playlist);
		radioPanel.setStyleName("area");
		radioPanel.setSizeFull();
		
		addComponent(header);
		addComponent(radioPanel);
	}
	
	@Override
	public void enter(ViewChangeEvent event)
	{
	}
}
