package org.enoy.awesomeradio.ui;

import org.enoy.awesomeradio.ui.view.ListenersView;
import org.enoy.awesomeradio.ui.view.NoConnectionView;
import org.enoy.awesomeradio.ui.view.PreferencesView;
import org.enoy.awesomeradio.ui.view.RadioView;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.annotation.VaadinUI;
import org.vaadin.spring.navigator.SpringViewProvider;
import com.netflix.appinfo.InstanceInfo.InstanceStatus;
import com.netflix.discovery.EurekaClient;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

@Theme("awesome")
@VaadinUI(path = "/*")
public class AwesomeRadioUI extends UI
{
	private static final long serialVersionUID = 736286929467685288L;
	public static final String NO_CONNECTION_VIEW = "noconnection";
	public static final String RADIO_VIEW = "radio";
	public static final String LISTENERS_VIEW = "listeners";
	public static final String PREFERENCES_VIEW = "preferences";
	
	@Autowired
    private EurekaClient client;
	
	@Autowired
	SpringViewProvider viewProvider;
	
	@Override
	protected void init(VaadinRequest request)
	{
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		mainLayout.setStyleName("root-layout navigation-template-dark");
		Responsive.makeResponsive(mainLayout);

		CssLayout headerBar = createHeaderBar();
		mainLayout.addComponent(headerBar);
		mainLayout.setExpandRatio(headerBar, 0);
		
		HorizontalLayout mainArea = createMainArea();
		VerticalLayout dashboard = new VerticalLayout();
		dashboard.setStyleName("dashboard-layout");
		mainArea.addComponent(dashboard);
		mainArea.setExpandRatio(dashboard, 1);
		mainLayout.addComponent(mainArea);
		mainLayout.setExpandRatio(mainArea, 1);
		
		setContent(mainLayout);
		
		Navigator navigator = new Navigator(this, dashboard);
        navigator.addProvider(viewProvider);
        navigator.addView(NO_CONNECTION_VIEW, NoConnectionView.class);
        navigator.addView(RADIO_VIEW, RadioView.class);
        navigator.addView(LISTENERS_VIEW, ListenersView.class);
        navigator.addView(PREFERENCES_VIEW, PreferencesView.class);
		
		if (!client.getInstanceRemoteStatus().equals(InstanceStatus.UP))
			navigator.navigateTo(NO_CONNECTION_VIEW);
		else
			navigator.navigateTo(RADIO_VIEW);
	}
	
	private CssLayout createHeaderBar()
	{
		CssLayout header_bar = new CssLayout();
		header_bar.setStyleName("header-bar");
		header_bar.setWidth("100%");
		
		NativeButton user_menu = new NativeButton();
		user_menu.setStyleName("user-menu");
		user_menu.setIcon(FontAwesome.USER);
		user_menu.setWidth("20px");
		user_menu.setHeight("20px");
		header_bar.addComponent(user_menu);
		
		Label user_name_label = new Label("John Smith");
		user_name_label.setStyleName("user-name-label");
		user_name_label.setWidth("79px");
		header_bar.addComponent(user_name_label);
		
		TextField search_field = new TextField();
		search_field.setStyleName("search-field");
		search_field.setIcon(FontAwesome.SEARCH);
		search_field.setInputPrompt("Search...");
		header_bar.addComponent(search_field);
		
		return header_bar;
	}
	
	private HorizontalLayout createMainArea()
	{
		HorizontalLayout mainArea = new HorizontalLayout();
		mainArea.setStyleName("main-area-layout");
		mainArea.setSizeFull();
		
		CssLayout side_bar = new CssLayout();
		side_bar.setStyleName("side-bar");
		side_bar.setHeight("100%");
		
		NativeButton sideRadioButton = new NativeButton(RADIO_VIEW);
		sideRadioButton.setStyleName("menu-button selected");
		sideRadioButton.setIcon(FontAwesome.MUSIC);
		sideRadioButton.setWidth("100%");
		sideRadioButton.addClickListener(this::selectSidebarButton);
		side_bar.addComponent(sideRadioButton);
		
		NativeButton sideListenersButton = new NativeButton(LISTENERS_VIEW);
		sideListenersButton.setStyleName("menu-button");
		sideListenersButton.setIcon(FontAwesome.USERS);
		sideListenersButton.setWidth("100%");
		sideListenersButton.addClickListener(this::selectSidebarButton);
		side_bar.addComponent(sideListenersButton);
		
		NativeButton sidePreferencesButton = new NativeButton(PREFERENCES_VIEW);
		sidePreferencesButton.setStyleName("menu-button");
		sidePreferencesButton.setIcon(FontAwesome.COGS);
		sidePreferencesButton.setWidth("100%");
		sidePreferencesButton.addClickListener(this::selectSidebarButton);
		side_bar.addComponent(sidePreferencesButton);
		
		mainArea.addComponent(side_bar);
		mainArea.setExpandRatio(side_bar, 0);
		
		Panel scroll_panel = new Panel();
		scroll_panel.setStyleName("scroll-panel");
		scroll_panel.setSizeFull();
		
		mainArea.addComponent(scroll_panel);
		mainArea.setExpandRatio(scroll_panel, 0);
		
		return mainArea;
	}
	
	private void selectSidebarButton(ClickEvent event)
	{
		event.getButton().getParent().forEach(component -> {
			if (component instanceof NativeButton)
				component.setStyleName("menu-button");
		});
		event.getButton().setStyleName("menu-button selected");
		
		getNavigator().navigateTo(event.getButton().getCaption());
	}
}
