package org.enoy.awesomeradio.ui.view;

import javax.annotation.PostConstruct;

import org.enoy.awesomeradio.ui.AwesomeRadioUI;
import org.vaadin.spring.navigator.annotation.VaadinView;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;

@VaadinView(name = AwesomeRadioUI.LISTENERS_VIEW)
public class ListenersView extends HorizontalLayout implements View
{
	private static final long serialVersionUID = -2796296693765732963L;

	@PostConstruct
	void init()
	{
		NativeButton b = new NativeButton("listenersView");
		addComponent(b);
	}
	
	@Override
	public void enter(ViewChangeEvent event)
	{
	}
}
